package com.example.books.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "book")
public class Book {

    @JsonProperty("book_id")
    @Field(type = FieldType.Text)
    private String id;
    private String book_title;
    private String book_author;
    private int book_pages;

    public Book() {
    }

    public Book(String book_id, String title, String author, int pages) {
        this.id = book_id;
        this.book_title = title;
        this.book_author = author;
        this.book_pages = pages;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBook_title() {
        return book_title;
    }

    public void setBook_title(String book_title) {
        this.book_title = book_title;
    }

    public String getBook_author() {
        return book_author;
    }

    public void setBook_author(String book_author) {
        this.book_author = book_author;
    }

    public int getBook_pages() {
        return book_pages;
    }

    public void setBook_pages(int book_pages) {
        this.book_pages = book_pages;
    }
}
