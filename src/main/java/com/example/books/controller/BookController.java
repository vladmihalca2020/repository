package com.example.books.controller;

import com.example.books.entity.Book;
import com.example.books.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping(value = "/getall", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Book> getAllBooks() throws IOException {
            return bookService.getAllBooksInfo();
    }

    @GetMapping(value = "/getall/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Book> getDataByName(@PathVariable String name) throws IOException {
            return  bookService.getBookByName(name);
    }

}
