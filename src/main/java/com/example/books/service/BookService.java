package com.example.books.service;

import com.example.books.entity.Book;
import com.example.books.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public List<Book> getAllBooksInfo() throws IOException {

        return bookRepository.findAllBooks();

    }

    public List<Book> getBookByName(String name) throws IOException {
        return bookRepository.findBookByName(name);
    }
}
