package com.example.books.repositoryImplementation;

import com.example.books.entity.Book;
import com.example.books.repository.BookRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.catalina.User;
import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class BookRepositoryImpl implements BookRepository {

    @Autowired
    private ObjectMapper objectMapper;

    RestHighLevelClient client =
            new RestHighLevelClient(RestClient.builder(new HttpHost("localhost", 9200, "http")));

    @Override
    public List<Book> findAllBooks() throws IOException {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("book");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchRequest.source(searchSourceBuilder);
        List<Book> bookList = new ArrayList<>();
        SearchResponse searchResponse = null;
        searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        if(searchResponse.getHits().getTotalHits().value > 0){
            SearchHit[] searchHits = searchResponse.getHits().getHits();
            for (SearchHit hit: searchHits) {
                Map<String, Object> map = hit.getSourceAsMap();
                bookList.add(objectMapper.convertValue(map, Book.class));
            }
        }
        return bookList;
    }

    @Override
    public List<Book> findBookByName(String name) throws IOException {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("book");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.boolQuery().must(QueryBuilders.termQuery("book_title.keyword",name)));
        searchRequest.source(searchSourceBuilder);
        List<Book> bookList = new ArrayList<>();

        SearchResponse searchResponse = null;
        searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        if(searchResponse.getHits().getTotalHits().value > 0){
            SearchHit[] searchHits = searchResponse.getHits().getHits();
            for (SearchHit hit: searchHits) {
                Map<String, Object> map = hit.getSourceAsMap();
                bookList.add(objectMapper.convertValue(map, Book.class));
            }
        }
        return bookList;
    }
}
