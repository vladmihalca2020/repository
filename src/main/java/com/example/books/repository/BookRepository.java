package com.example.books.repository;

import com.example.books.entity.Book;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.List;

@Repository
public interface BookRepository {

    List<Book> findAllBooks() throws IOException;

    List<Book> findBookByName(String name) throws IOException;
}
